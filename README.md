# AspectJS-demo-memo

This project is a simple demo showing the capabilities of the [@AspectJS/memo] package.
It memoizes a recurvive fibonacci method to multiply performances by over 1000.
