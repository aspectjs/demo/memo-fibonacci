import { Memo } from "@aspectjs/memo";

export class Demo {
  static fibonacci(num: number) {
    if (num < 2) {
      return num;
    } else {
      return Demo.fibonacci(num - 1) + Demo.fibonacci(num - 2);
    }
  }
  @Memo()
  static memoizedFibonacci(num: number) {
    if (num < 2) {
      return num;
    } else {
      return Demo.memoizedFibonacci(num - 1) + Demo.memoizedFibonacci(num - 2);
    }
  }
}
