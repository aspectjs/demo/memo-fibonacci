import { getWeaver } from "@aspectjs/core";
import { MemoAspect } from "@aspectjs/memo";
import { Demo } from "./fibonacci";

getWeaver().enable(new MemoAspect());

function main() {
  let [time, result] = getExecutionTime(() => Demo.fibonacci(40));
  console.log(`Demo.fibonacci(40) returned ${result} after ${time} ms`);

  [time, result] = getExecutionTime(() => Demo.memoizedFibonacci(40));
  console.log(`Demo.memoizedFibonacci(40) returned ${result} after ${time} ms`);

  [time, result] = getExecutionTime(() => Demo.memoizedFibonacci(40));
  console.log(`Demo.memoizedFibonacci(40) returned ${result} after ${time} ms`);
}

main();

function getExecutionTime(fn: () => unknown) {
  let t = new Date().getTime();
  let result = fn();
  let elapsedTime = new Date().getTime() - t;
  return [elapsedTime, result];
}
